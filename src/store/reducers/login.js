import * as loginActions from '../actions/actionTypes';

const initialState = {
    idToken: null,
    userId: null,
    error: null,
    loading: false
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case loginActions.LOGIN_START:
            return {
                ...state,
                error: null,
                loading: true
            };
        case loginActions.LOGIN_SUCCESS:
            return {
                ...state,
                idToken: action.idToken,
                userId: action.userId,
                loading: false
            };
        case loginActions.LOGIN_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false
            }
        case loginActions.LOGOUT:
            return {
                ...state,
                idToken: null,
                userId: null
            }
        default:
            return state;
    }
}

export default reducer;