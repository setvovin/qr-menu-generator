import * as actionTypes from '../actions/actionTypes';

const initialState = {
    dishes: []
}

const reducer = ( state = initialState, action ) => {

    switch (action.type) {
        case actionTypes.ADD_DISH:
            let addedDishes = [...state.dishes, action.dishes];

            return {
                ...state,
                dishes: addedDishes
            };
        case actionTypes.DELETE_DISH:
            return {
                ...state,
                dishes: action.dishes.dishes
            };
        default:
            return state;
    }
}

export default reducer;