import * as actionTypes from '../actions/actionTypes';

const initialState = {
    menus: [],
    error: '',
    loading: false,
    created: false
}

const reducer = (state = initialState, action) => {

    switch (action) {
        case actionTypes.CREATE_MENU_INIT:
            return {
                ...state,
                loading: false,
                created: false
            };
        case actionTypes.CREATE_MENU_START:
            return {
                ...state,
                loading: true
            };
        case actionTypes.CREATE_MENU_FAIL:
            return {
                ...state,
                error: action.error
            };
        case actionTypes.CREATE_MENU_SUCCESS:
            const newMenu = {
                ...action.menu,
                id: action.menuId
            };

            return {
                ...state,
                loading: false,
                created: true,
                menus: state.menus.concat ( newMenu )
            };
        default:
            return {
                ...state
            }
    }
}

export default reducer;