import * as registerActions from './actionTypes';

import axios from 'axios';

export const registerUserInit = () => {
    return {
        type: registerActions.REGISTER_USER_INIT
    }
};

export const registerUserStart = () => {
    return {
        type: registerActions.REGISTER_USER_START
    }
};

export const registerUserSuccess = userData => {
    return {
        type: registerActions.REGISTER_USER_SUCCESS,
        userData: userData
    }
};

export const registerUserFail = error => {
    return {
        type: registerActions.REGISTER_USER_FAIL,
        error: error
    }
};

export const registerUser = registerData => {
    return dispatch => {
        dispatch( registerUserStart() );

        const data = {
            email: registerData.email,
            password: registerData.password,
            returnSecureToken: true
        }

        axios.post('https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyA6LBIpl2ogS1OltqyBdmIshqbfJ5uyH3I', data)
            .then(
                response => {
                    dispatch( registerUserSuccess( response.data) )
                }
            )
            .catch(
                error => {
                    dispatch( registerUserFail( error ))
                }
            );
    }
}