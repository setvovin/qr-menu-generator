import * as actionTypes from './actionTypes';
import axios from '../../axios-builder';

export const createMenuInit = () => {
    return {
        type: actionTypes.CREATE_MENU_INIT
    }
}

export const createMenuFail = error => {
    return {
        type: actionTypes.CREATE_MENU_FAIL,
        error: error
    }
}

export const createMenusSuccess = menu => {
    return {
        type: actionTypes.CREATE_MENU_SUCCESS,
        menu: menu
    }
}

export const createMenuStart = () => {
    return {
        type: actionTypes.CREATE_MENU_START
    }
}

export const createMenu = ( menu, _props ) => {

    return dispatch => {
        dispatch( createMenuStart() );

        axios.post('/menus.json?auth=' + _props.token, menu)
        .then( response => {
            dispatch( createMenusSuccess(response.data.name, menu) );
        })
        .then ( _props.history.push('/') )
        .catch ( error => {
            dispatch ( createMenuFail(error));
        });
    }
}