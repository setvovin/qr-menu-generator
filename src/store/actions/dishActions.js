import * as actionTypes from './actionTypes';

export const addDish = ( dishes ) => {
    return {
        type: actionTypes.ADD_DISH,
        dishes: dishes
    }
};

export const deleteDish = ( dishes ) => {
    return {
        type: actionTypes.DELETE_DISH,
        dishes: dishes
    }
};