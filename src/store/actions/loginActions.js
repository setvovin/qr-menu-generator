import * as loginActions from './actionTypes';
import axios from 'axios';

export const loginStart = () => {
    return {
        type: loginActions.LOGIN_START
    }
};

export const loginSuccess = (idToken, userId) => {
    return {
        type: loginActions.LOGIN_SUCCESS,
        idToken: idToken,
        userId: userId
    }
};

export const loginFail = error => {
    return {
        type: loginActions.LOGIN_FAIL,
        error: error
    }
};

export const logout = () => {
    return {
        type: loginActions.LOGOUT
    }
};

export const checkAuthTimeout = ( timeout ) => {
    return dispatch => {
        setTimeout(() => {
            dispatch( logout() );
        }, timeout * 1000);
    }
};

export const login = (email, password) => {
    return dispatch => {
        dispatch( loginStart() );

        const loginData = {
            email: email,
            password: password,
            returnSecureToken: true
        }

        axios.post('https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyA6LBIpl2ogS1OltqyBdmIshqbfJ5uyH3I', loginData)
            .then(
                response => {
                    dispatch( loginSuccess( response.data.idToken, response.data.localId) );
                    dispatch ( checkAuthTimeout( response.data.expiresIn) );
                }
            )
            .catch(
                error => {
                    dispatch( loginFail( error.response.data.error ));
                }
            )
    }
};