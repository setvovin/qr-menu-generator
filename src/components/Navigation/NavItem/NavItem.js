import React from 'react';
import { NavLink } from 'react-router-dom';
import classes from './NavItem.module.css';

import clasess from './NavItem.module.css';

const navItem = ( props ) => {
    return (
        props.isAuth ? <li className={classes.NavItem}>
            <NavLink 
                to={props.link}
                exact={props.exact}
                activeClassName={clasess.active}
            > {props.children} </NavLink>
        </li>: null
    );
}

export default navItem;