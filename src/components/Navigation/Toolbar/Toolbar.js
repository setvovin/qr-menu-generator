import React from 'react';
import NavItems from '../../../containers/NavItems/NavItems';
import Logo from '../../../components/Logo/Logo';

import menuLogo from '../../../assets/Logo/menu.svg';

import classes from './Toolbar.module.css';

const toolbar = () => {
    return (
        <header>
            <nav className={classes.Toolbar}>
                <Logo source={menuLogo} />
                <NavItems />
            </nav>
        </header>
    )
}

export default toolbar;