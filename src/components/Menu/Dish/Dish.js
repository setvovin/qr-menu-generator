import React from 'react';

import Button from '../../UI/Button/Button';
import classes from './Dish.module.css';

const dish = props => {
    return (
        <div className={classes.Dish} id={props.id}>
            <h4 className={classes.Title}>{props.title}</h4>
            <span>{props.image}</span>
            <span>{props.description}</span>
            <span>{props.price}</span>
            <Button
                buttonType="DeleteDish"
                id="DeleteDish-id"
                name="DeleteDish"
                action={props.delete}
            ></Button>
        </div>
    )
}

export default dish;
