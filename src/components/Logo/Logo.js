import React from 'react';

import classes from './Logo.module.css';

const logo = ( props ) => {
    return (
            <img className={classes.Logo} src={props.source} alt="Logo" />
    )
}

export default logo;