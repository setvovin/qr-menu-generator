import React from 'react';

import classes from './Input.module.css';

const input = ( props ) => {

    const acceptedFormatImages = [".jpg", ".png", ".jpge"];

    let inputTag = null;
    let inputClass = [];

    if (!props.isValid) inputClass.push(classes.Invalid);

    switch (props.elementType) {
        case "input":
            inputClass.push(classes.Element);
            inputTag = (
                <div className={classes.Input}>
                    <label
                        htmlFor={props.id}>{props.title}</label>
                    <input 
                        className={inputClass.join(' ')}
                        type={props.type}
                        id={props.id}
                        name={props.name}
                        placeholder={props.placeholder}
                        value={props.value}
                        onChange={props.change}
                        accept={acceptedFormatImages}></input>
                </div>
                );
            break;
        case "text-area":
            inputClass.push(classes.TextArea);
            inputTag = (
                <div className={classes.Input}>
                    <label
                        htmlFor={props.id}>{props.title}</label>
                    <textarea
                        className={inputClass.join(' ')}
                        id={props.id}
                        name={props.name}
                        placeholder={props.placeholder}
                        onChange={props.change}></textarea>
                </div>
            );
            break;
        default:
            inputClass.push(classes.Element);
            inputTag = (
                <div className={classes.Input}>
                    <label
                        htmlFor={props.id}>{props.title}</label>
                    <input 
                        className={inputClass.join(' ')}
                        type={props.type}
                        id={props.id}
                        name={props.name}
                        placeholder={props.placeholder}
                        value={props.value}
                        onChange={props.change}></input>
                </div>
            );
            break;
    }

    return (
        inputTag
        );
}

export default input;