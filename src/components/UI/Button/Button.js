import React from 'react';

import classes from './Button.module.css';

const button = props => {

    const type = (props && props.type) || 'button'; 

    return (
        <div>
            <button className={[classes.Button, classes[props.buttonType]].join(' ') }
                id={props.id}
                name={props.name}
                disabled={props.disabled}
                onClick={props.action}
                type={type}>
                    {props.value}
                </button>
        </div>
    )
};

export default button;