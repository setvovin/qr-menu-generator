import React from 'react'

import classes from './spinner.module.css';

const spinner = () => {
    return <div className={classes.Spinner}></div>;
}

export default spinner;