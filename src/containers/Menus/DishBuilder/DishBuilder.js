import React from 'react';

import Dish from '../../../components/Menu/Dish/Dish';
import Input from '../../../components/UI/Input/input';
import Button from '../../../components/UI/Button/Button';
import classes from './DishBuilder.module.css';

const dishes = props => {

        let dishes = null;
        let form = null;
        const inputDishes = [];

        if (props.addedDishes) {
            dishes = props.addedDishes.dishes.map( (dish, id) => {
                return (
                    <Dish
                        key = {id}
                        id = {id}
                        title={dish.nameValue}
                        image={dish.photoValue}
                        description={dish.descriptionValue}
                        price={dish.priceValue}
                        delete={props.deleteDish}></Dish>
                )
            });
        }

        for (let input in props.inputs) {
            inputDishes.push({
                id: input,
                config: props.inputs[input]
            });
        }

        form = inputDishes.map( input => {
            return <Input
                key={input.id}
                elementType={input.config.elementType}
                type={input.config.elementProperties.type}
                id={input.id}
                title={input.config.elementProperties.title}
                name={input.config.elementProperties.name}
                placeholder={input.config.elementProperties.placeholder}
                value={input.config.elementProperties.value}
                isValid={input.config.isValid}
                change={props.onChangeHandler}
            ></Input>
        });

        return (
            <div className={classes.Dishes}>
                <h3 className={classes.Title}>Platos</h3>
                {form}
                <Button
                    buttonType="Add"
                    name="add-button"
                    disabled={!props.formValidated}
                    action={props.addDish}
                    ></Button>
                {dishes}
            </div>
        )
    }

export default dishes;
