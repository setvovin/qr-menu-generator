import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import DishBuilder from '../DishBuilder/DishBuilder';
import Input from '../../../components/UI/Input/input';
import Button from '../../../components/UI/Button/Button';

import classes from './Menu.module.css';

import * as actions from '../../../store/actions/index';
import { connect } from 'react-redux';

class Menu extends Component {

    state = {
        inputs: {
            menu: {
                elementType: 'input',
                elementProperties: {
                    type: 'text',
                    id: 'menu-id',
                    name: 'menu',
                    title: 'Menú',
                    placeholder: 'Agregar un titulo para tu menú'
                },
                value: '',
                validate: true,
                validationRules: {
                    minlenght: 20,
                    maxlenght: 100,
                    required: true
                },
                isValid: false
            },
            portada: {
                elementType: 'input',
                elementProperties: {
                    type: 'file',
                    id: 'portada-id',
                    name: 'portada',
                    title: 'Portada'
                },
                value: '',
                validate: true,
                validationRules: {
                    imageFormats: ["jpg", "png", "jpge"]
                },
                isValid: true
            },
            description: {
                elementType: 'text-area',
                elementProperties: {
                    type: '',
                    id: 'description-id',
                    name: 'description',
                    title: 'Descripción',
                    placeholder: 'Agrega una descripción del menú'
                },
                value: '',
                validate: true,
                validationRules: {
                    maxlenght: 500
                },
                isValid: true
            }
        },
        inputDishes: {
            photo: {
                elementType: 'input',
                elementProperties: 
                {
                    type: 'file',
                    title: 'Foto',
                    id: 'photo-id',
                    name: 'photo',
                    placeholder: '',
                    value: ''
                },
                validation: true,
                validationRules: {
                    imageFormat: ["jpg", "png", "jpge"]
                },
                isValid: true
            },
            name: {
                elementType: 'input',
                elementProperties: {
                    type: 'text',
                    title: 'Nombre',
                    id: 'name-id',
                    name: 'name',
                    placeholder: 'Nombre del plato',
                    value: ''
                },
                validation: true,
                validationRules: {
                    minwidth: 20,
                    maxwidth: 100,
                    required: true
                },
                isValid: false
            },
            description: {
                elementType: 'text-area',
                elementProperties: {
                    type: '',
                    title: 'Descripción',
                    id: 'dish-description-id',
                    name: 'description',
                    placeholder: 'Agrega la descripción del plato',
                    value: ''
                },
                validation: true,
                validationRules: {
                    maxwidth: 500
                },
                isValid: true
            },
            price: {
                elementType: 'input',
                elementProperties: {
                    type: 'number',
                    title: 'Precio',
                    id: 'price-id',
                    name: 'price',
                    placeholder: '0',
                    value: ''
                },
                validation: true,
                validationRules: {
                    required: true
                },
                isValid: false
            }
        },
        dishFormValidated: false,
        formValidated: false
    }

    onSubmitHandler = ( event ) => {
        event.preventDefault();

        let menu = {};
        const form = {};

        for (let input in this.state.inputs) {
            form[input] = this.state.inputs[input].value;
        }

        menu = {
            ...form,
            ...this.props.dishes.dishes
        }
        
        this.props.onCreateMenu(menu, this.props);
    }

    addDishHandler = () => {

        const updatedDish = {...this.state.inputDishes};
        const inputs = {...this.state.inputs};
        let newDish = {};

        for (let dish in updatedDish) {
            newDish = {
                ...newDish,
                [dish]: updatedDish[dish].elementProperties.value
            }

            updatedDish[dish].elementProperties.value = '';
        };

        const dishes = {
            photoValue: newDish.photo,
            nameValue: newDish.name,
            descriptionValue: newDish.description,
            priceValue: newDish.price
        };

        let formIsValid = true;

        for (let input in inputs) {
            formIsValid = inputs[input].isValid && formIsValid;
        }

        this.setState({formValidated: formIsValid});

        this.props.onAddDish(dishes);
    }

    deleteDishHandler = ( event ) => {

        const dish = [...this.props.dishes.dishes];

        const updatedDish = dish.splice(event.target.id, 1);
        const newDishes = {...this.props.dishes, dishes: dish};

        const formIsValid = newDishes.dishes.length > 0 && this.state.formValidated;

        this.setState({formValidated: formIsValid});

        this.props.onDeleteDish(newDishes);
    }

    checkValidity ( value, rules ) {
        let isValid = true;

        if (!rules) return;

        if (rules.required) isValid = value.trim() !== '' && isValid;

        if (rules.minwidth) isValid = value.length >= rules.minwidth && isValid;

        if (rules.maxwidth) isValid = value.length <= rules.maxwidth && isValid;

        if (rules.imageFormat) isValid = rules.imageFormat.includes(value.split('.')[1]) && isValid;

        return isValid;
    }

    onDishesInputChangeHandler ( event ) {

        const updatedDishesInputs = {...this.state.inputDishes};
        const updatedDishInput = {...this.state.inputDishes[event.target.id]};

        updatedDishInput.elementProperties.value = event.target.value;
        updatedDishInput.isValid = this.checkValidity(event.target.value, updatedDishInput.validationRules);
        updatedDishesInputs[event.target.id] = updatedDishInput;

        let formIsValid = true;

        for (let input in updatedDishesInputs) {
            formIsValid = updatedDishesInputs[input].isValid && formIsValid;
        }

        this.setState({inputDishes: updatedDishesInputs, dishFormValidated: formIsValid});
    }

    onChangeHandler ( event, key ) {
        const updatedForm = {...this.state.inputs};
        const updatedInput = {...this.state.inputs[key]};

        updatedInput.value = event.target.value;
        updatedInput.isValid = this.checkValidity(updatedInput.value, updatedInput.validationRules);
        updatedForm[key] = updatedInput;

        let formIsValid = true;

        for (let input in updatedForm) {
            formIsValid = updatedForm[input].isValid && formIsValid;
        }

        formIsValid = this.props.dishes.dishes.length > 0 && formIsValid;

        this.setState({inputs: updatedForm, formValidated: formIsValid });
    }

    render () {

        const form = Object.keys(this.state.inputs).map( key => {
            
            return <Input
                        key={key}
                        elementType={this.state.inputs[key].elementType}
                        type={this.state.inputs[key].elementProperties.type}
                        id={this.state.inputs[key].elementProperties.id}
                        name={this.state.inputs[key].elementProperties.name}
                        title={this.state.inputs[key].elementProperties.title}
                        placeholder={this.state.inputs[key].elementProperties.placeholder}
                        isValid={this.state.inputs[key].isValid}
                        change={(event) => this.onChangeHandler(event, key)}
                    >
                    </Input>
        });

        return (
            <form onSubmit={this.onSubmitHandler} className={classes.Menu}>
                {form}
                <DishBuilder
                    inputs={this.state.inputDishes}
                    addedDishes={this.props.dishes}
                    onChangeHandler={(event) => this.onDishesInputChangeHandler(event)}
                    formValidated={this.state.dishFormValidated}
                    addDish={this.addDishHandler}
                    deleteDish={this.deleteDishHandler} />
                <div className={classes.ButtonSection}>
                    <Button
                        buttonType="Cancel"
                        id="cancel-id"
                        name="cancel"
                        value="Cancel"
                    ></Button>
                    <Button
                        buttonType="Success"
                        id="submit-id"
                        name="submit"
                        value="Guardar"
                        disabled={!this.state.formValidated}
                        type="submit"
                    ></Button>
                </div>
            </form>
    )}
}

const mapStateToPros = state => {
    return {
        dishes: state.dishes,
        menus: state.menus.menus,
        loading: state.menus.loading,
        created: state.menus.created,
        token: state.login.idToken
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAddDish: dishes => dispatch(actions.addDish(dishes)),
        onDeleteDish: dishes => dispatch(actions.deleteDish(dishes)),
        onCreateMenu: (menu, _props) => dispatch(actions.createMenu(menu, _props))
    }
}

export default connect(mapStateToPros, mapDispatchToProps) (withRouter(Menu))
