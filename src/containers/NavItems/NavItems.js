import React, { Component } from 'react';
import { connect } from 'react-redux';
import NavItem from '../../components/Navigation/NavItem/NavItem';

import classes from './NavItems.module.css';

class navItems extends Component {    
    
    render() {
        const navigation = process.env.REACT_APP_MENU_ITEMS.split(' ').map( menuItem => {
            const auth = (this.props.isAuth && menuItem.includes('*')) 
                            || (!this.props.isAuth && menuItem.includes('*'))
                            || !menuItem.includes('*');

            return  <NavItem
                        isAuth={auth}
                        link={`/${menuItem.replace('*', '')}`}
                        exact
                        key={menuItem.replace('*', '')}>
                        {menuItem.replace('*', '')}
                    </NavItem>
        });

        return (
            <ul className={classes.NavItems}>
                {navigation}
            </ul>
        );
    }
}

const mapStateToProps = state => {
    return {
        isAuth: state.login.idToken !== null
    }
}

export default connect(mapStateToProps, null) (navItems);