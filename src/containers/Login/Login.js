import React, { Component } from 'react';

import Input from '../../components/UI/Input/input';
import Button from '../../components/UI/Button/Button';
import Spinner from '../../components/UI/Spinner/spinner';
import * as actions from '../../store/actions/index';

import classes from './Login.module.css';
import { connect } from 'react-redux';

class Login extends Component {

    state = {
        inputs: {
            email: {
                elementType: 'input',
                elementProperties: {
                    type: 'email',
                    name: 'email',
                    placeholder: 'Escribe tu correo electronico',
                    title: 'Correo Electronico',
                    value: ''
                },
                validationRules: {
                    required: true,
                    isEmail: true
                },
                isValid: false
            },
            password: {
                elementType: 'input',
                elementProperties: {
                    type: 'password',
                    name: 'password',
                    title: 'Password',
                    value: ''
                },
                validationRules: {
                    required: true,
                    minlength: 8
                },
                isValid: false
            }
        },
        buttons: {
            login: {
                type: 'submit',
                buttonType: 'Success',
                id: 'login-id',
                login: 'submit',
                value: 'Entrar'
            }
        },
        formIsValid: false
    };

    onSubmitLogin =  ( event ) => {

        event.preventDefault();

        this.props.onLogin(this.state.inputs.email.elementProperties.value, this.state.inputs.password.elementProperties.value);
    }

    checkValidity = (value, rules) => {
        let isValid = true;
        const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;

        if (!rules) return isValid;

        if (rules.required) isValid = value.trim() !== '' && isValid;

        if (rules.isEmail) isValid = pattern.test(value) && isValid;

        if (rules.minlength) isValid = value.length >= rules.minlength && isValid;

        return isValid;
    }

    onChangeValue = ( event, key ) => {
        const updateInputs = {...this.state.inputs};
        const updateInput = {...this.state.inputs[key]};

        updateInput.elementProperties.value = event.target.value;
        updateInput.isValid = this.checkValidity(event.target.value, updateInput.validationRules);

        updateInputs[key] = updateInput;

        let formIsValid = true;

        for (let updateInput in updateInputs) {
            formIsValid = updateInputs[updateInput].isValid && formIsValid;
        }

        this.setState({inputs: updateInputs, formIsValid: formIsValid});
    }

    render() {

        const inputs = [];
        let errorMessage = null;

        for (let input in this.state.inputs) {
            inputs.push ({
                    id: input,
                    config: this.state.inputs[input]
            });
        }

        let form = (
                    inputs.map ( input => {
                        return <Input
                            key={input.id}
                            elementType={input.config.elementType}
                            type={input.config.elementProperties.type}
                            id={input.id}
                            title={input.config.elementProperties.title}
                            name={input.config.elementProperties.name}
                            placeholder={input.config.elementProperties.placeholder}
                            value={input.config.elementProperties.value}
                            change={(event) => this.onChangeValue (event, input.id)}
                            isValid={input.config.isValid}
                        ></Input>
                    })
        )

        if (this.props.loading) form = <Spinner />;

        if (this.props.error) {
            errorMessage = (
                <p>{this.props.error.message}</p>
            );
        }

        return (
            <div className={classes.Login}>
                <h1>Login</h1>
                {errorMessage}
                <form onSubmit={this.onSubmitLogin} className={classes.LoginForm}>
                    {form}
                    <Button
                        type={this.state.buttons.login.type}
                        buttonType={this.state.buttons.login.buttonType}
                        id={this.state.buttons.login.id}
                        name={this.state.buttons.login.name}
                        disabled={!this.state.formIsValid}
                        value={this.state.buttons.login.value}
                    ></Button>
                </form>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        loading: state.login.loading,
        error: state.login.error
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onLogin: (email, password) => dispatch( actions.login(email, password) )
    }
}

export default connect( mapStateToProps, mapDispatchToProps ) (Login);