import React, { Component } from 'react'

class Register extends Component {

    state = {
        inputs: {
            name: {
                elementType: 'input',
                elementProperties: {
                    type: 'text',
                    id: 'name-id',
                    name: 'name',
                    placeholder: 'Nombre del restaurante',
                    value: ''
                },
                validate: true,
                validateRules: {
                    required: true,
                    maxlength: 100
                },
                isValid: false
            },
            description: {
                elementType: 'text-area',
                elementProperties: {
                    id: 'description-id',
                    name: 'description',
                    placeholder: 'Descripción del restaurante',
                    value: ''
                },
                validate: false,
                validateRules: {},
                isValid: true
            },
            email: {
                elementType: 'input',
                elementProperties: {
                    type: 'email',
                    id: 'email-id',
                    name: 'email',
                    placeholder: 'Email del restaurante',
                    value: ''
                },
                validate: true,
                validateRules: {
                    required: true,
                    isEmail: ''
                },
                isValid: false
            },
            address: {
                elementType: 'input',
                elementProperties: {
                    type: 'text',
                    id: 'address-id',
                    name: 'address',
                    placeholder: 'Dirección principal del restaurante',
                    value: ''
                },
                validate: true,
                validateRules: {
                    required: true
                },
                isValid: false
            },
            phone: {
                elementType: 'input',
                elementProperties: {
                    type: 'text',
                    id: 'phone-id',
                    name: 'phone',
                    placeholder: 'Número de teléfono',
                    value: ''
                },
                validate: true,
                validateRules: {
                    required: true,
                    maxlength: 10
                },
                isValid: false
            },
            password: {
                elementType: 'input',
                elementProperties: {
                    type: 'password',
                    id: 'password-id',
                    name: 'password',
                    value: ''
                },
                validate: true,
                validateRules: {
                    required: true,
                    minlength: 8,
                    pattern: ''
                },
                isValid: false
            },
            confirmPassword: {
                elementType: 'input',
                elementProperties: {
                    type: 'password',
                    id: 'confirmPassword-id',
                    name: 'confirmPassword',
                    value: ''
                },
                validate: true,
                validateRules: {
                    confirm: true
                },
                isValid: false
            }
        },
        formIsValid: false
    }

    render() {
        return (
            <div>
                
            </div>
        )
    }
}

export default Register;