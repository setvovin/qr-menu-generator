import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import './App.css';

import HomePage from './components/Home/Home';
import Menus from './containers/Menus/Menus';
import CreateMenu from './components/Menu/CreateMenu/CreateMenu';
import Login from './containers/Login/Login';
import Layout from './hoc/Layout/Layout';
import Aux from './hoc/Auxiliary/Auxiliary';

class App extends Component {
  
  render () {
    return (
      <Aux>
        <Layout>        
          <Route path={"/Menus"} component={Menus} />
          <Route path={"/Crear"} component={CreateMenu} />
          <Route path={"/login"} component={Login} />
          <Route path={"/"} exact component={HomePage} />
        </Layout>
      </Aux>
    );
  }
}

export default App;
