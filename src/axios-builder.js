import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://qr-generator-builder.firebaseio.com/'
});

export default instance;